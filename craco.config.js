const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#3388C9',
              '@btn-default-color': '#3388C9',
              '@btn-default-border': '#3388C9',
              '@btn-border-radius-base': '5px',
              '@text-color': '#444444',
              '@heading-color': '#000000',
              '@input-height-base': '20px',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
