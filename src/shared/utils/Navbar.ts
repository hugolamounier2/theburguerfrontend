import { NavbarItems } from "../../shared/components/PrivateLayout/Navbar/index";
import { ECommonRoutes } from "../routes/routeList";
import { faChartPie, faTable } from "@fortawesome/free-solid-svg-icons";

export const NavbarItemsTree: NavbarItems[] = [
  { title: "Dashboard", icon: faChartPie, href: ECommonRoutes.dashboard},
  { title: "Comandas", icon: faTable, href: ECommonRoutes.ticket},
];
