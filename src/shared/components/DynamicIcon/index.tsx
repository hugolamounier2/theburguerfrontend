import React, { Suspense, lazy } from 'react';
import { AntdIconProps } from '@ant-design/icons/lib/components/AntdIcon';
import { colors } from '../../../styles/palette';

type DynamicIconProps = {
  iconName: string;
};

export default function DynamicIcon({
  iconName,
  style,
  color,
}: DynamicIconProps & AntdIconProps): JSX.Element {
  const hasIcon =
    typeof iconName === 'string' ? iconName : 'ExclamationCircleOutlined';
  const hasColor =
    typeof color === 'string' ? color : colors.neutral.black;
  const Icon = lazy(() => import(`@ant-design/icons/es/icons/${hasIcon}.js`));

  return (
    <>
      <Suspense fallback={<></>}>
        <Icon
          style={{
            ...style,
            color: hasColor,
          }}
        />
      </Suspense>
    </>
  );
}
