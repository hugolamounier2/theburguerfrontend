import styled from "styled-components";

export const PageHeadingContainer = styled.div`
  width: 98%;
  margin: 0 1% 0 1%;
  position: sticky;
  top:10px;
  z-index: 10;

  .page_header {
    background: #546e7aCC;
    backdrop-filter: blur(6px);
    border-radius: 1rem;
  }

  .ant-breadcrumb {
    padding-bottom: 0.5rem;
    border-bottom: 1px solid ${({ theme }) => theme.colors.dark.blueGrayDark};
    & .ant-breadcrumb-link {
      color: ${({ theme }) => theme.colors.neutral.white};
    }
    & a.ant-breadcrumb-link:hover {
      color: ${({ theme }) => theme.colors.main.orange};
    }
  }

  .ant-page-header-heading-title {
    text-shadow: 1px 1px 2px rgba(150, 150, 150, 1);
    font-size: 1.5rem;
    font-weight: bold;
    color: ${({ theme }) => theme.colors.neutral.white};
    padding-right: 1rem;
  }
  .ant-page-header-back-button {
    font-size: 1.5rem;
    color: ${({ theme }) => theme.colors.neutral.white};
    &:hover {
      color: ${({ theme }) => theme.colors.main.orange};
    }
  }
`;
