import React from "react";
import { Breadcrumb, PageHeader, PageHeaderProps } from "antd";
import { PageHeadingContainer } from "./styles";
import { ArrowLeftOutlined, SwapRightOutlined } from "@ant-design/icons";
import { HomeOutlined } from "@ant-design/icons";
import { ECommonRoutes } from "../../routes/routeList";

export type TRouteTree = {
  title: string;
  href: string;
};

interface PageHeadingProps {
  routeTree: TRouteTree[];
}

export default function PageHeading({
  title,
  routeTree,
}: PageHeaderProps & PageHeadingProps): JSX.Element {
  const breadcrumbRender = (
    props: PageHeaderProps,
    defaultDom: React.ReactNode
  ): React.ReactNode => {
    return (
      <Breadcrumb separator={<SwapRightOutlined />}>
        <Breadcrumb.Item href={ECommonRoutes.dashboard}>
          <HomeOutlined />
        </Breadcrumb.Item>
        {routeTree.map((route, index, array) => (
          <>
            {array.length - 1 === index ? (
              <Breadcrumb.Item>{route.title}</Breadcrumb.Item>
            ) : (
              <Breadcrumb.Item href={route.href}>{route.title}</Breadcrumb.Item>
            )}
          </>
        ))}
      </Breadcrumb>
    );
  };

  return (
    <PageHeadingContainer>
      <PageHeader
        className="page_header"
        title={title}
        onBack={() => window.history.back()}
        backIcon={<ArrowLeftOutlined />}
        breadcrumbRender={breadcrumbRender}
      />
    </PageHeadingContainer>
  );
}
