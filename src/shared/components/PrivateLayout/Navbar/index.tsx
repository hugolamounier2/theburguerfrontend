import React from "react";
import { NavbarContainer } from "./styles";
import TheBurguerLogo from "../../../../assets/brand/brand_logo.svg";
import { Button } from "antd";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconProp } from "@fortawesome/fontawesome-svg-core";

export type NavbarItems = {
  title: string;
  icon: IconProp;
  href?: string;
  subItems?: Pick<NavbarItems, "title" | "href">[];
};

interface NavbarProps {
  items: NavbarItems[];
}

export default function Navbar({ items }: NavbarProps): JSX.Element {
  return (
    <NavbarContainer>
      <img className="logo" src={TheBurguerLogo} alt="The Burguer Logo" />
      <ul>
        {items?.map((item) => (
          <>
            {item.href ? (
              <Link to={item?.href}>
                {window.location.pathname === item.href ? (
                  <li className="active">
                    <i><FontAwesomeIcon icon={item.icon} /></i>
                    <Button type="link">{item.title}</Button>
                  </li>
                ) : (
                  <li>
                    <i><FontAwesomeIcon icon={item.icon} /></i>
                    <Button type="link">{item.title}</Button>
                  </li>
                )}
              </Link>
            ) : (
              <li>
                <i><FontAwesomeIcon icon={item.icon} /></i>
                <Button type="link">{item.title}</Button>
              </li>
            )}
          </>
        ))}
      </ul>
    </NavbarContainer>
  );
}
