import styled from "styled-components";

export const NavbarContainer = styled.nav`
  width: 100px;
  height: 100vh;
  background: ${({ theme }) => theme.colors.neutral.white};
  box-shadow: -2px 0px 20px rgba(0, 0, 0, 0.15);
  display: flex;
  flex-direction: column;
  align-items: center;
  transition: ease 0.3s;
  z-index: 100;

  .logo {
    width: 70px;
    margin: 1rem 0 1rem 0;
    transition: ease 0.3s;
  }

  ul {
    width: 100%;
    margin: 0;
    padding: 0;
    list-style-type: none;
    & li {
      box-sizing: content-box;
      transition: ease 0.3s;
      display: flex;
      cursor: pointer;
      padding: 0.5em 2.5em 0.3em 2.5em;
      margin: 0 0 1rem 0;
      color: ${({ theme }) => theme.colors.neutral.black};
      background: ${({ theme }) => theme.colors.neutral.mediumGray};
      & i {
        display: flex;
        transition: ease 0.3s;
        font-size: 2em;
        margin:0;
      }
      & button {
        visibility: hidden;
        overflow: hidden;
        & span {
          color: transparent;
        }
      }
      & ul {
        display: none;
      }
    }
    & li:hover {
      background: ${({ theme }) => theme.colors.main.orange};
      & i {
        color: ${({ theme }) => theme.colors.neutral.white} !important;
      }
    }
    & li.active {
      background: ${({ theme }) => theme.colors.main.orange};
      & i {
        color: ${({ theme }) => theme.colors.neutral.white} !important;
      }
    }
  }

  &:hover {
    width: 250px;
    ul {
      width: 100%;
      margin: 0;
      padding: 0;
      list-style-type: none;
      & li {
        border-radius: 0.5rem;
        transition: ease 0.3s;
        display: flex;
        cursor: pointer;
        padding: 0.5em 1em 0.3em 1em;
        margin: 0 0.5em 1rem 0.5em;
        color: ${({ theme }) => theme.colors.neutral.black};
        background: ${({ theme }) => theme.colors.neutral.mediumGray};
        & i {
          display: flex;
          transition: ease 0.3s;
          font-size: 2em;
        }
        & button {
          width: fit-content;
          visibility: visible;
          & span {
            color: ${({ theme }) => theme.colors.neutral.black};
          }
        }
        & ul {
          display: none;
        }
      }
      & li:hover {
        background: ${({ theme }) => theme.colors.main.orange};
        & .anticon {
          color: ${({ theme }) => theme.colors.neutral.white};
        }
        & button {
          width: fit-content;
          visibility: visible;
          & span {
            font-weight: bold;
            color: ${({ theme }) => theme.colors.neutral.white};
          }
        }
      }
      & li.active {
        background: ${({ theme }) => theme.colors.main.orange};
        & i {
          color: ${({ theme }) => theme.colors.neutral.white} !important;
        }
        & button {
          & span {
            color: ${({ theme }) => theme.colors.neutral.white};
            font-weight: bold;
          }
        }
      }
    }
  }
`;
