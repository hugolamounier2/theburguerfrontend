import styled from "styled-components";

export const PrivateLayoutContainer = styled.div`
  display: flex;

  .wrapper {
      width: 100%;
      position: relative;
      overflow-y: auto;
      & .content{
          width: 100%;
          padding: 0 2rem 0 2rem;
          height: calc(100vh - 200px);
          margin: -150px 0 0 0;
      }
  }
}
`;
