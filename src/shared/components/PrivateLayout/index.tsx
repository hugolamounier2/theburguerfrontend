import React, { ReactNode } from "react";
import { PrivateLayoutContainer } from "./styles";
import Navbar from "./Navbar";
import Topbar from "./Topbar";
import { NavbarItemsTree } from "../../utils/Navbar";

interface PrivateLayoutProps {
  children: ReactNode;
}

export default function PrivateLayout({
  children,
}: PrivateLayoutProps): JSX.Element {
  return (
    <PrivateLayoutContainer>
      <Navbar items={NavbarItemsTree} />
      <div className="wrapper">
        <Topbar />
        <div className="content">{children}</div>
      </div>
    </PrivateLayoutContainer>
  );
}
