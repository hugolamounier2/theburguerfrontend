import React from "react";
import { Card } from "antd";
import { DefaultContainerWrapper } from "./styles";

interface DefaultContainerProps {
  children: React.ReactNode;
}

export default function DefaultContainer({
  children,
}: DefaultContainerProps): JSX.Element {
  return (
    <DefaultContainerWrapper>
      <Card>{children}</Card>
    </DefaultContainerWrapper>
  );
}
