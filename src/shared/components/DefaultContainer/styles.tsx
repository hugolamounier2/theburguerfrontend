import styled from "styled-components";

export const DefaultContainerWrapper = styled.div`
  position: relative;
  margin: -3rem 0 0 0;
  z-index: 5;

  .ant-card {
    min-height: 500px;
    border-radius: 0.5rem;
    & .ant-card-body{
        padding-top: 4rem;
    }
  }
`;
