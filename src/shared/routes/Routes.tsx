import { Route, Switch } from "react-router-dom";
import Example from "../../features/Example";
import Ticket from "../../features/Ticket";
import { ECommonRoutes } from "./routeList";

export default function Routes(): JSX.Element {
    return (
        <Switch>
            <Route exact component={Example} path="/" />
            <Route exact component={Ticket} path={ECommonRoutes.ticket} />
        </Switch>
    );
}