import { notification } from "antd";
import { TOrder } from "../models/Order/Order";
import apiService from "./axiosInstances/apiService";

export default class OrderService {
  public static async createOrder() {
    try {
      const { data } = await apiService.post("/order");
      return Promise.resolve(data);
    } catch (e) {
      notification.open({
        type: "error",
        message:
          "Não foi possível criar um novo pedido. Por favor, tente novamente.",
      });
      Promise.reject();
    }
  }
  public static async getAllOrders(): Promise<TOrder[]> {
    try {
      const { data } = await apiService.get("/order");
      return Promise.resolve(data);
    } catch (e) {
      notification.open({
        type: "error",
        message:
          "Ocorreu um erro ao obter as solicitações. Por favor, tente novamente.",
      });
      return Promise.reject();
    }
  }
}
