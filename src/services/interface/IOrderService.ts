import { Moment } from "moment";

export type CreateOrderResponse = {
    id: number;
    createdAt: Moment;
}