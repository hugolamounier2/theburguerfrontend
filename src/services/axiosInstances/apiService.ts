import axios from 'axios';

const apiService = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

export const setRequestInterceptor = () => {
  apiService.interceptors.request.use(async (config) => {
    const newConfig = { ...config };
    // if (accessToken) newConfig.headers.Authorization = `Bearer ${accessToken}`;

    newConfig.headers['Access-Control-Allow-Origin'] = '*';
    newConfig.headers['Access-Control-Allow-Headers'] =
      'Origin, X-Requested-With, Content-Type, Accept';
    newConfig.headers['Content-Type'] = 'application/json';

    return newConfig;
  });
};

apiService.interceptors.response.use(
  async (response) => {
    return response;
  },
  (error) => {
    return new Promise((resolve, reject) => {
      return reject(error);
    });
  }
);

export default apiService;