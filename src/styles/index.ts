import { DefaultTheme } from "styled-components";
import { colors } from "./palette";

const theme: DefaultTheme = {
    colors
};

export default theme;
