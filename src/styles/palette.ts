export const colors = {
  neutral: {
    lightGray: "#fafafa",
    mediumGray: "#F6F6F6",
    black: "#444444",
    white: "#ffffff",
  },
  main: {
    orange: "#F7931E",
  },
  light: {
    blueGray: "#b0bec5",
  },
  dark: {
    blueGray: "#607d8b",
    blueGrayDark: "#455a64",
  },
  actions: {
    dark: {
      warning: "#FAAD14",
    },
    light: {
      info: "#69C0FF",
      warning: "#FFF566",
      error: "#ff4d4f",
      success: "#D3F261",
      selected: "#f9fef5",
    },
  },
};
