import { Moment } from "moment"
import { Product } from "../Product/Product"

export type TOrder = {
    id: number;
    status?: number;
    customerId?: number;
    products?: Product[];
    details?: string;
    discount?: number;
    createdAt: Moment;
}