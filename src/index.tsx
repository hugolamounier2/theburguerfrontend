import React from "react";
import ReactDOM from "react-dom";
import "./index.less";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import ptBr from "antd/lib/locale/pt_BR";
import { ConfigProvider } from "antd";
import theme from "./styles";
import App from "./App";

ReactDOM.render(
  <BrowserRouter>
    <ConfigProvider locale={ptBr}>
      <ThemeProvider theme={theme}>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </ThemeProvider>
    </ConfigProvider>
  </BrowserRouter>,
  document.getElementById("root")
);
