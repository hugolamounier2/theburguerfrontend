import { PlusOutlined } from "@ant-design/icons";
import { Button, Empty } from "antd";
import Title from "antd/lib/typography/Title";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { TOrder } from "../../models/Order/Order";
import DefaultContainer from "../../shared/components/DefaultContainer";
import { FlexContainer } from "../../shared/components/Grid";
import PageHeading, { TRouteTree } from "../../shared/components/PageHeading";
import PrivateLayout from "../../shared/components/PrivateLayout";
import Order from "./components/Order";
import useTicketController from "./controller/useTicketController";
import { TicketContainer } from "./styles";

export default function Ticket(): JSX.Element {
  const pageTitle = "Comandas";
  const routeTree: TRouteTree[] = [{ title: pageTitle, href: "" }];
  const { orders, isLoadingOrders, createOrder, getOrders } =
    useTicketController();

  useEffect(() => {
    getOrders();
  }, []);

  const handleAddOrder = () => {
    createOrder();
  };
  const handleDeleteOrder = (orderId: number) => {
    // setTickets(tickets.filter((order) => order.id !== orderId));
  };

  const emptyState = (
    <Empty
      image={Empty.PRESENTED_IMAGE_SIMPLE}
      imageStyle={{
        height: 60,
      }}
      description={
        <span>
          Nenhuma comanda <b>em aberto</b>
        </span>
      }
    >
      <Button
        type="primary"
        onClick={() => {
          handleAddOrder();
        }}
      >
        <PlusOutlined />
        <span>Adicionar</span>
      </Button>
    </Empty>
  );

  const isLoadingSkeleton = (
    <FlexContainer flexDirection="row" flexWrap="wrap" justifyContent="center">
      <Order isLoading={true} data={{} as TOrder} />
      <Order isLoading={true} data={{} as TOrder} />
      <Order isLoading={true} data={{} as TOrder} />
      <Order isLoading={true} data={{} as TOrder} />
    </FlexContainer>
  );

  return (
    <PrivateLayout>
      <PageHeading title={pageTitle} routeTree={routeTree} />
      <DefaultContainer>
        <FlexContainer flexDirection="row" justifyContent="space-between">
          <Title level={3}>Em atendimento</Title>
          <Button
            onClick={() => {
              handleAddOrder();
            }}
          >
            <PlusOutlined />
            <span>Adicionar</span>
          </Button>
        </FlexContainer>
        <TicketContainer>
          {isLoadingOrders ? (
            isLoadingSkeleton
          ) : (
            <>
              {orders.length <= 0 ? emptyState : null}
              <FlexContainer
                flexDirection="row"
                flexWrap="wrap"
                justifyContent="center"
              >
                {orders.filter(order => order.status === 1)
                  ?.sort((a, b) => {
                    return a.id > b.id ? -1 : 1;
                  })
                  .map((order) => (
                    <Order
                      key={order.id}
                      data={order}
                      onDelete={() => handleDeleteOrder(order.id)}
                    />
                  ))}
              </FlexContainer>
            </>
          )}
        </TicketContainer>
      </DefaultContainer>
    </PrivateLayout>
  );
}
