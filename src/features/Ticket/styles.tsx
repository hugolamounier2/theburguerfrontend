import styled from "styled-components";

export const TicketContainer = styled.div`
  padding: 1em;
  background: #eceff1;
  border-radius: 0.5rem;
`;
