import React, { useState } from "react";
import { TOrder } from "../../../models/Order/Order";
import { CreateOrderResponse } from "../../../services/interface/IOrderService";
import OrderService from "../../../services/OrderService";

export default function useTicketController() {
  const [orders, setOrders] = useState<TOrder[]>([]);
  const [isLoadingOrders, setIsLoadingOrders] = useState<boolean>(false);

  async function getOrders() {
    setIsLoadingOrders(true);
    await OrderService.getAllOrders().then((data: TOrder[]) => {
      setOrders([...orders, ...data]);
      setIsLoadingOrders(false);
    });
  }

  async function createOrder() {
    await OrderService.createOrder().then((data: CreateOrderResponse) => {
      const newOrder: TOrder = {
        id: data.id,
        createdAt: data.createdAt,
      };
      setOrders([...orders, newOrder]);
    });
  }

  return {
    orders,
    isLoadingOrders,
    setOrders,
    createOrder,
    getOrders,
  };
}
