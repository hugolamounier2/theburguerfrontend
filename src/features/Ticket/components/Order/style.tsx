import styled from "styled-components";

export const OrderContainer = styled.div`
  display: flex;
  .ant-card {
    background: #f9fbe7;
    border-color: #e0e0e0;
    border-radius: 0.2rem;
    margin: 0 1em 1em 1em;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.05);
    min-height: 300px;
    width: 350px;

    & .ant-card-head {
      border-color: #e0e0e0;
    }

    & .ant-card-body {
      padding: 1em 1em 5em 1em;
    }

    & .ant-card-actions {
      width: 100%;
      position: absolute;
      bottom: 0;
      background: #f9fbe7;
      border-color: #e0e0e0;
      & li {
        border-color: #e0e0e0;
      }
    }
  }
  .order_date {
    font-size: 0.8rem;
    padding: 0.2em 0.8em;
    border-radius: 10px;
    color: ${({ theme }) => theme.colors.neutral.white};
    background: ${({ theme }) => theme.colors.dark.blueGray};
    & .anticon {
      margin-right: 0.5rem;
    }
  }
  .anticon-minus-circle {
    color: ${({ theme }) => theme.colors.actions.light.error};
  }
  .anticon-delete:hover {
    color: ${({ theme }) => theme.colors.actions.light.error} !important;
  }
  .product_list {
    padding: 0.5rem;
    border-radius: 0.5rem;
    background: #eceff1;
  }
  .ant-form-item-label {
    padding: 0;
  }
`;
