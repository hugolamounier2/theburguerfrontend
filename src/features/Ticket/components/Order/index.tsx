import React, { useState } from "react";
import { OrderContainer } from "./style";
import { Button, Card, Form, Input, Select, Skeleton } from "antd";
import {
  CalendarOutlined,
  DeleteOutlined,
  EditOutlined,
  MinusCircleOutlined,
  PlusCircleOutlined,
  PlusOutlined,
  SaveOutlined,
} from "@ant-design/icons";
import { FlexContainer } from "../../../../shared/components/Grid";
import { TOrder } from "../../../../models/Order/Order";
import moment from "moment";

interface OrderProps {
  data: TOrder;
  onDelete?: (e: any) => void;
  isLoading?: boolean;
}

const { Option } = Select;

export default function Order({
  data,
  onDelete,
  isLoading = false,
}: OrderProps): JSX.Element {
  const [isEditing, setIsEditing] = useState<boolean>(false);

  const cardExtra = (
    <div className="order_date">
      <CalendarOutlined />
      <span>{moment(data.createdAt).format("DD/MM/YY HH:mm")}</span>
    </div>
  );

  const editingLayout = (
    <Form layout="vertical">
      <FlexContainer alignItems="flex-end">
        <Form.Item
          name="costumerId"
          label={<h5>Cliente</h5>}
          hasFeedback
          style={{ flex: 1 }}
        >
          <Select
            showSearch
            placeholder="Selecione o cliente"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
          >
            <Option value="1">Not Identified</Option>
            <Option value="2">Closed</Option>
            <Option value="3">Communicated</Option>
            <Option value="4">Identified</Option>
            <Option value="5">Resolved</Option>
            <Option value="6">Cancelled</Option>
          </Select>
        </Form.Item>
        <Form.Item style={{ padding: "0 0 0 0.7rem" }}>
          <Button type="link" style={{ padding: "1rem 0" }}>
            <PlusCircleOutlined />
          </Button>
        </Form.Item>
      </FlexContainer>
      <Form.Item style={{ margin: 0 }}>
        <h5>Produtos</h5>
      </Form.Item>
      <div className="product_list">
        <Form.List name="products">
          {(fields, { add, remove }) => (
            <>
              {fields.map(({ key, name, fieldKey, ...restField }) => (
                <FlexContainer
                  flexDirection="row"
                  alignItems="center"
                  style={{ marginBottom: "0.5rem" }}
                >
                  <Form.Item
                    style={{
                      flex: "0 0 30px",
                      margin: "0 0.25rem 0 0",
                    }}
                    {...restField}
                    name={[name, "numProduct"]}
                    fieldKey={[fieldKey]}
                    initialValue={1}
                    rules={[{ required: true, message: "*" }]}
                  >
                    <Input
                      style={{
                        padding: "0 0.5em",
                        fontSize: "0.8rem",
                        textAlign: "center",
                        height: "20px",
                      }}
                    ></Input>
                  </Form.Item>
                  <Form.Item
                    style={{
                      flex: "1 0 auto",
                      margin: "0 0.5rem 0 0",
                    }}
                    {...restField}
                    name={[name, "product"]}
                    fieldKey={[fieldKey]}
                    rules={[{ required: true, message: "Selecione o produto" }]}
                  >
                    <Select
                      showSearch
                      placeholder="Selecione um produto"
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option?.children
                          .toLowerCase()
                          .indexOf(input.toLowerCase()) >= 0
                      }
                      filterSort={(optionA, optionB) =>
                        optionA.children
                          .toLowerCase()
                          .localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      <Option value="1">Not Identified</Option>
                      <Option value="2">Closed</Option>
                      <Option value="3">Communicated</Option>
                      <Option value="4">Identified</Option>
                      <Option value="5">Resolved</Option>
                      <Option value="6">Cancelled</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item style={{ margin: "0" }}>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Form.Item>
                </FlexContainer>
              ))}
              <Form.Item style={{ margin: 0 }}>
                <Button
                  size="small"
                  type="dashed"
                  block
                  onClick={() => add()}
                  icon={<PlusOutlined />}
                >
                  Adicionar produto
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      </div>
    </Form>
  );

  const returnEditSaveIcon = () => {
    if (!isEditing)
      return (
        <EditOutlined
          onClick={() => {
            setIsEditing(true);
          }}
          key="edit"
        />
      );
    return (
      <SaveOutlined
        onClick={() => {
          setIsEditing(false);
        }}
        key="edit"
      />
    );
  };

  return (
    <OrderContainer id={"orderId_" + data.id} key={data.id}>
      {isLoading ? (
        <Card
          bordered
        >
          <Skeleton active title paragraph={{ rows: 6, width: '100%' }} />
          {isEditing ? editingLayout : null}
        </Card>
      ) : (
        <Card
          extra={cardExtra}
          actions={[
            returnEditSaveIcon(),
            <DeleteOutlined onClick={onDelete} key="delete" />,
          ]}
          bordered
          title={"#" + data.id}
        >
          {isEditing ? editingLayout : null}
        </Card>
      )}
    </OrderContainer>
  );
}
