import React from "react";
import PrivateLayout from "../../shared/components/PrivateLayout";

export default function Example(): JSX.Element {
    return(
        <PrivateLayout>
            <div>123</div>
        </PrivateLayout>
    );
}